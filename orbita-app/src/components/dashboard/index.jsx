import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchSolarPanelsDashboard } from '../../store/actions/solar-panels.api';

import './dashboard.css';

const monthNames = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 
'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

const formatDate = (date) => {

  const weekDay = ['Segunda-Feira', 'Terça-Feira', 'Quarta-Feira', 
    'Quinta-Feira', 'Sexta-Feira', 'Sábado', 'Domingo'];

  const monthDate = date.getDate();
  const monthIndex = date.getMonth();
  const year = date.getFullYear();
  const day = date.getDay();

  return `${weekDay[day]}, ${monthDate} de ${monthNames[monthIndex]} de ${year}`;
};

const toLocaleNumberString = (value) => {
  return new Number(value).toLocaleString('pt-BR')
};

const Dashboard = ({ accessToken, fetchSolarPanelsDashboard, dashboardInfo }) => {
  const { greaterCostInstallation, top3MonthInstallations, 
    totalSystemSizeByYear, totalValue } = dashboardInfo;

  useEffect(() => {
    fetchSolarPanelsDashboard(accessToken);
  }, []);

  return (
    <div>
      <h1 className="dashboard-header">{formatDate(new Date())}</h1>

      <div className="dashboard-container">
        <div className="dashboard-block gradient-color-yellow">
          <h3 className="block-header">Instalações realizadas</h3>
          <span className="block-title">Estado</span>
          <span>{totalValue.state}</span>
          <span className="block-title">Valor total da instalação</span>
          <span>R$ {toLocaleNumberString(totalValue.totalValue)}</span>
        </div>

        <div className="dashboard-block gradient-color-blue">
          <h3 className="block-header">Maior custo de instalação</h3>
          <span className="block-title">CEP</span>
          <span>{greaterCostInstallation.zipCode}</span>
          <span className="block-title">Custo da instalação</span>
          <span>R$ {toLocaleNumberString(greaterCostInstallation.cost)}</span>
        </div>

        <div className="dashboard-block gradient-color-purple">
          <h3 className="block-header">Meses com mais instalações</h3>

          <div className="block-child">
            <div className="block-title">Mês</div>
            <div className="block-title">Total de instalações</div>
          </div>

          {top3MonthInstallations.map((installation, i) => (
            <div key={i} className="block-child">
              <span>{monthNames[installation.month]}</span>
              <span>{toLocaleNumberString(installation.count)}</span>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  accessToken: state.accessToken,
  dashboardInfo: state.dashboardInfo,
});
const mapDispatchToProps = dispatch => bindActionCreators({ fetchSolarPanelsDashboard }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
