import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Input } from '../input/index';
import { fetchLogin } from '../../store/actions/auth.api';
import { createNewUser } from '../../store/actions/user.api';

import './login.css';

const Login = ({ createNewUser, fetchLogin }) => {
  const login = () => {
    const login = {
      username: document.getElementById('email').value,
      password: document.getElementById('password').value,
    };

    fetchLogin(login);
  };

  return (
    <div className="container">
      <h1 className="header">Login</h1>

      <Input label="E-mail:" name="email" type="email" />

      <Input label="Senha:" name="password" type="password" />

      <button type="button" className="button" onClick={login}>Entrar</button>

      <a href="#" onClick={() => createNewUser()}>Criar conta</a>
    </div>
  );
}

const mapStateToProps = state => ({ });
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchLogin,
  createNewUser,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
