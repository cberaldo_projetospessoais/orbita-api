import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Input } from '../input/index';
import { fetchCreateUser } from '../../store/actions/user.api';

const NewUser = ({ fetchCreateUser }) => {
  const createUser = () => {
    const user = {
      name: document.getElementById('name').value,
      email: document.getElementById('email').value,
      password: document.getElementById('password').value,
      state: document.getElementById('state').value,
    };

    fetchCreateUser(user);
  };

  return (
    <div>
      <h1>Criar usuário</h1>

      <Input label="Name:" name="name" type="text" />

      <Input label="E-mail:" name="email" type="email" />

      <Input label="Senha:" name="password" type="password" />

      <Input label="Estado:" name="state" type="text" />

      <button type="button" className="button" onClick={createUser}>Criar</button>
    </div>
  );
};

const mapStateToProps = state => ({ 
  isCreatingNewUser: state.isCreatingNewUser,
});
const mapDispatchToProps = dispatch => bindActionCreators({ fetchCreateUser }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NewUser);
