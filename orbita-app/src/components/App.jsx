import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Dashboard from './dashboard';
import Login from './login';
import NewUser from './new-user';

import '../index.css'

const App = ({ isLoggedIn, isCreatingNewUser }) => {
  if (isLoggedIn) {
    return <Dashboard />;
  }

  if (isCreatingNewUser) {
    return <NewUser />
  }

  return <Login />;
};

const mapStateToProps = state => ({
  isLoggedIn: state.isLoggedIn,
  isCreatingNewUser: state.isCreatingNewUser,
});
const mapDispatchToProps = dispatch => bindActionCreators({ }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
