import React from 'react';

import './input.css';

export const Input = ({ type, name, label }) => (
  <div>
    <label htmlFor={name}>{label}</label>
    <input id={name} name={name} type={type} className="custom-input" />
  </div>
);
