
export const handlerErrors = (data) => {
  if (data.error) {
    return Promise.reject({ message: data.error });
  }

  return Promise.resolve(data);
};

export const handleResponse = (response) => {
  return response.json();
};

export const options = {
  "method": "GET",
  "crossDomain": true,
  "headers": {
    "Content-Type": "application/json",
    "Accept": "*/*",
  }
};
