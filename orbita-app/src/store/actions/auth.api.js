
import { options, handleResponse, handlerErrors } from './handlers.api';
import { AUTH_API } from './types';

const searchAlertApiUrl = 'http://localhost:3000/auth';

const fetchLoginSuccess = ({ access_token }) => ({
  type: AUTH_API.FETCH_LOGIN_SUCCESS,
  access_token
});

const fetchLoginError = ({ }) => ({
  type: AUTH_API.FETCH_LOGIN_ERROR,
});

export const fetchLogin = (login) => (dispatch) => {
  return fetch(searchAlertApiUrl, {
      ...options,
      method: 'POST',
      body: JSON.stringify(login)
    })
    .then(res => handleResponse(res))
    .then(data => handlerErrors(data))
    .then(login => dispatch(fetchLoginSuccess(login)))
    .catch(error => dispatch(fetchLoginError(error)))
};
