
import { options, handleResponse, handlerErrors } from './handlers.api';
import { fetchLogin } from './auth.api';
import { USER_API } from './types'

const searchAlertApiUrl = 'http://localhost:3000/users';

const fetchCreateUserError = ({ }) => ({
  type: USER_API.FETCH_CREATE_USER_ERROR,
});

export const fetchCreateUser = (user) => (dispatch) => {
  return fetch(searchAlertApiUrl, {
      ...options,
      method: 'POST',
      body: JSON.stringify(user)
    })
    .then(res => handleResponse(res))
    .then(data => handlerErrors(data))
    .then(user => dispatch(fetchLogin({
      username: user.email,
      password: user.password,
    })))
    .catch(error => dispatch(fetchCreateUserError(error)))
};

export const createNewUser = () => ({
  type: USER_API.CREATE_NEW_USER,
  isCreatingNewUser: true,
});

export const cancelCreateNewUser = () => ({
  type: USER_API.CREATE_NEW_USER,
  isCreatingNewUser: false,
});
