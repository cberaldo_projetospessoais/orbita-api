
export const AUTH_API = {
  FETCH_LOGIN_SUCCESS: 'FETCH_LOGIN_SUCCESS',
  FETCH_LOGIN_ERROR: 'FETCH_LOGIN_ERROR',
};

export const SOLAR_PANEL_API = {
  FETCH_SOLAR_PANEL_DASHBOARD_SUCCESS: 'FETCH_SOLAR_PANEL_DASHBOARD_SUCCESS',
  FETCH_SOLAR_PANEL_DASHBOARD_ERROR: 'FETCH_SOLAR_PANEL_DASHBOARD_ERROR',
};

export const USER_API = {
  CREATE_NEW_USER: 'CREATE_NEW_USER',
}