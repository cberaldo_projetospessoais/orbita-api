
import { handlerErrors, handleResponse, options } from './handlers.api';
import { SOLAR_PANEL_API } from './types';

const searchAlertApiUrl = 'http://localhost:3000/solar-panels';

const fetchSolarPanelsDashboardSuccess = (dashboardInfo) => ({
  type: SOLAR_PANEL_API.FETCH_SOLAR_PANEL_DASHBOARD_SUCCESS,
  dashboardInfo
});

const fetchSolarPanelsDashboardError = ({ }) => ({
  type: SOLAR_PANEL_API.FETCH_SOLAR_PANEL_DASHBOARD_ERROR,
});

export const fetchSolarPanelsDashboard = (accessToken) => (dispatch) => {
  return fetch(searchAlertApiUrl, {
      ...options,
      headers: {
        ...options.header,
        Authorization: `Bearer ${accessToken}`
      }
    })
    .then(res => handleResponse(res))
    .then(data => handlerErrors(data))
    .then(dashboardInfo => dispatch(fetchSolarPanelsDashboardSuccess(dashboardInfo)))
    .catch(error => dispatch(fetchSolarPanelsDashboardError(error)));
};
