import { AUTH_API, USER_API, SOLAR_PANEL_API } from '../actions/types';

const DEFAULT_DASHBOARD = {
  totalValue: {
    totalValue: 0,
    state: '',
  },
  greaterCostInstallation: {
    zipCode: '',
    cost: 0,
  },
  top3MonthInstallations: [],
};

const DEFAULT_SETTINGS = {
  isLoggedIn: false,
  isCreatingNewUser: false,
  accessToken: undefined,
  dashboardInfo: DEFAULT_DASHBOARD,
}

const rootReducer = (state = DEFAULT_SETTINGS, action) => {
  switch (action.type) {
    case AUTH_API.FETCH_LOGIN_SUCCESS:
      return { ...state, accessToken: action.access_token, isLoggedIn: true };
    case AUTH_API.FETCH_LOGIN_ERROR:
      return { ...state, accessToken: undefined, isLoggedIn: true };
    case SOLAR_PANEL_API.FETCH_SOLAR_PANEL_DASHBOARD_SUCCESS:
      return { ...state, dashboardInfo: action.dashboardInfo };
    case SOLAR_PANEL_API.FETCH_SOLAR_PANEL_DASHBOARD_ERROR:
      return { ...state, dashboardInfo: DEFAULT_DASHBOARD };
    case USER_API.CREATE_NEW_USER:
      return { ...state, isCreatingNewUser: action.isCreatingNewUser };
    default:
      return state;
  }
};

export default rootReducer
