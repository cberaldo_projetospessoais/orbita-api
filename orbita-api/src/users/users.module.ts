import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/database/database.module';
import { UsersController } from './users.controller';
import { userProvider } from './users.provider';
import { UsersService } from './users.service';

const providers = [
  ...userProvider,
  UsersService,
];

@Module({
  providers,
  imports: [DatabaseModule],
  controllers: [UsersController],
  exports: providers,
})
export class UsersModule {}
