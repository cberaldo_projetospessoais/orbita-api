import { Controller, Get, Param, Post, Delete, Put, Body } from '@nestjs/common';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  /**
   * Inject controller dependencies.
   * @param {UsersService} usersService injected users services.
   */
  constructor(private readonly usersService: UsersService) { }

  @Get('/')
  getUsers() {
    return this.usersService.getUsers();
  }

  @Get('/:id')
  getUser(@Param('id') id) {
    return this.usersService.getUser(id);
  }

  @Post('/')
  createUser(@Body() user) {
    return this.usersService.createUser(user);
  }

  @Put('/:id')
  updateUser(@Param('id') id, @Body() user) {
    return this.usersService.updateUser(id, user);
  }

  @Delete('/:id')
  deleteUser(@Param('id') id) {
    return this.usersService.deleteUser(id);
  }
}
