import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  name: string;

  @Column({ length: 200 })
  password: string;

  @Index({ unique: true })
  @Column({ length: 300 })
  email: string;

  @Column({ length: 2 })
  state: string;
}
