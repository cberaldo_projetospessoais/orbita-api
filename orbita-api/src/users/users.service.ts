import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from './users.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  /**
   * Inject service dependencies.
   * @param {Repository<User>} usersRepository users access to database repository.
   */
  constructor(
    @Inject('USER_REPOSITORY')
    private readonly usersRepository: Repository<User>,
  ) { }

  /**
   * Return a user entity.
   * @param id users id.
   */
  async getUser(id: number): Promise<User> {
    const user = await this.usersRepository.findOne(id);
    return user;
  }

  /**
   * Return all users.
   */
  async getUsers(): Promise<User[]> {
    const users = await this.usersRepository.find();
    return users;
  }

  /**
   * Create a user entity in the database.
   * @param user user entity to be created.
   */
  async createUser(user): Promise<User> {
    user.password = await this.encrypt(user.password);
    const created = await this.usersRepository.save(user);
    return created;
  }

  /**
   * Update a user entity in the database.
   * @param id users id.
   * @param user users entity.
   */
  async updateUser(id: number, user): Promise<object> {
    const updated = await this.usersRepository.update(id, user);
    return updated;
  }

  /**
   * Delete a user entity from the database.
   * @param id users id.
   */
  async deleteUser(id: number): Promise<User> {
    const user = await this.getUser(id);

    if (!user) {
      return new User();
    }

    const removed = this.usersRepository.remove(user);
    return removed;
  }

  /**
   * Find a user by its e-mail.
   * @param email user e-mail
   */
  async findUserByEmail(email): Promise<User> {
    return await this.usersRepository.findOne({ where: { email } });
  }

  /**
   * Compare password to hash and validate user access.
   * @param hash password hash loaded from database.
   * @param password password informed by the user.
   */
  async validatePassword(hash, password) {
    return new Promise((resolve) => {
      bcrypt.compare(password, hash, (err, res) => {
        resolve(res);
      });
    });
  }

  /**
   * Encrypt information using bcrypt.
   * @param password user password.
   */
  private async encrypt(password) {
    return new Promise((resolve) => {
      bcrypt.hash(password, 10, (err, hash) => {
        resolve(hash);
      });
    });
  }
}
