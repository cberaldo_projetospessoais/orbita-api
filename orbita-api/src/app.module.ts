import { Module } from '@nestjs/common';
import { UsersController } from './users/users.controller';
import { DatabaseModule } from './database/database.module';
import { userProvider } from './users/users.provider';
import { UsersService } from './users/users.service';
import { SolarPanelController } from './solar-panels/solar-panel.controller';
import { SolarPanelService } from './solar-panels/solar-panel.service';
import { solarPanelProvider } from './solar-panels/solar-panel.provider';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    DatabaseModule,
    UsersModule,
    AuthModule,
  ],
  controllers: [
    SolarPanelController,
  ],
  providers: [
    ...solarPanelProvider,
    SolarPanelService,
  ],
})
export class AppModule {}
