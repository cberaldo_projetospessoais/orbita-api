import { Connection } from 'typeorm';
import { SolarPanel } from './solar-panel.entity';

export const solarPanelProvider = [
  {
    provide: 'SOLAR_PANEL_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(SolarPanel),
    inject: ['DATABASE_CONNECTION'],
  },
];
