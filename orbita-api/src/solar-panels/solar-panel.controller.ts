import { Controller, Get, Post, Put, Param, Body, Delete, UseGuards, Req, Res, HttpStatus } from '@nestjs/common';
import { SolarPanelService } from './solar-panel.service';
import { AuthGuard } from '@nestjs/passport';
import { SolarPanel } from './solar-panel.entity';
import { sendUnauthorizedResponse } from 'src/auth/unauthorized.response';

@Controller('solar-panels')
export class SolarPanelController {
   /**
   * Inject controller dependencies.
   * @param {SolarPanelService} solarPanelService injected solar panel services.
   */
  constructor(private readonly solarPanelService: SolarPanelService) { }

  @Get('/:year')
  @UseGuards(AuthGuard('jwt'))
  async getDashboardInformation(@Req() req) {
    const solarPanels = await this.solarPanelService.getSolarPanels(req.user.state);

    const totalValue = this.solarPanelService.getTotalInstallationCost(solarPanels);

    const greaterCost = this.solarPanelService.getGreaterCostInstallation(solarPanels);

    const top3MonthInstallations = this.solarPanelService.getTop3MonthInstallations(solarPanels);

    const totalSystemSizeByYear = this.solarPanelService.getTotalSystemSizeByYear(solarPanels);

    return {
      top3MonthInstallations,
      totalSystemSizeByYear,
      totalValue: {
        totalValue,
        state: req.user.state,
      },
      greaterCostInstallation: {
        cost: greaterCost.cost,
        zipCode: greaterCost.zipCode,
      },
    };
  }

  @Get('/:id')
  @UseGuards(AuthGuard('jwt'))
  async getSolarPanel(@Req() req, @Res() res, @Param('id') id) {
    const hasAccessTo = await this.solarPanelService.hasAccessTo(id, req.user.state);
    if (!hasAccessTo) {
      sendUnauthorizedResponse(res);
      return;
    }

    const solarPanel = await this.solarPanelService.getSolarPanel(id);
    res.json(solarPanel);
  }

  @Post('/')
  @UseGuards(AuthGuard('jwt'))
  createSolarPanel(@Body() solarPanel) {
    return this.solarPanelService.createSolarPanel(solarPanel);
  }

  @Put('/:id')
  @UseGuards(AuthGuard('jwt'))
  async updateSolarPanel(@Req() req, @Res() res, @Param('id') id, @Body() solarPanel) {
    const hasAccessTo = await this.solarPanelService.hasAccessTo(id, req.user.state);
    if (!hasAccessTo) {
      sendUnauthorizedResponse(res);
      return;
    }

    const updated = await this.solarPanelService.updateSolarPanel(id, solarPanel);
    res.json(updated);
  }

  @Delete('/:id')
  @UseGuards(AuthGuard('jwt'))
  async deleteSolarPanel(@Req() req, @Res() res, @Param('id') id) {
    const hasAccessTo = await this.solarPanelService.hasAccessTo(id, req.user.state);
    if (!hasAccessTo) {
      sendUnauthorizedResponse(res);
      return;
    }

    const deleted = this.solarPanelService.deleteSolarPanel(id);
    res.json(deleted);
  }
}
