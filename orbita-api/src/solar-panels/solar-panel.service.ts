import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { SolarPanel } from 'src/solar-panels/solar-panel.entity';

@Injectable()
export class SolarPanelService {
  constructor(
    @Inject('SOLAR_PANEL_REPOSITORY')
    private readonly solarPanelRepository: Repository<SolarPanel>,
  ) { }

  /**
   * Return all solar panels entities for the specified state.
   * @param state the solar panels state.
   */
  async getSolarPanels(state: string): Promise<SolarPanel[]> {
    const solarPanels = await this.solarPanelRepository.find({ where: { state } });
    return solarPanels;
  }

  /**
   * Return a solar panel entity.
   * @param id solar panel id.
   */
  async getSolarPanel(id: number): Promise<SolarPanel> {
    const solarPanels = await this.solarPanelRepository.findOne(id);
    return solarPanels;
  }

  /**
   * Create a solar panel entity in the database.
   * @param user solar panel entity to be created.
   */
  async createSolarPanel(solarPanel: SolarPanel): Promise<SolarPanel> {
    const created = await this.solarPanelRepository.save(solarPanel);
    return created;
  }

  /**
   * Update a solar panel entity in the database.
   * @param id solar panel id.
   * @param solarPanel solar panel entity.
   */
  async updateSolarPanel(id: number, solarPanel: SolarPanel): Promise<object> {
    const updated = await this.solarPanelRepository.update(id, solarPanel);
    return updated;
  }

  /**
   * Delete a solar panel entity from the database.
   * @param id solar panel id.
   */
  async deleteSolarPanel(id: number): Promise<SolarPanel> {
    const solarPanel = await this.getSolarPanel(id);

    if (!solarPanel) {
      return new SolarPanel();
    }

    const removed = this.solarPanelRepository.remove(solarPanel);
    return removed;
  }

  /**
   * Validate user access to the solar panel.
   * @param id solar panel id.
   * @param state user state.
   */
  async hasAccessTo(id: number, state: string) {
    const solarPanel = await this.getSolarPanel(id);

    if (!solarPanel) {
      return false;
    }

    if (solarPanel.state !== state) {
      return false;
    }

    return true;
  }

  /**
   * Return the total installation cost of the array provided.
   * @param {SolarPanel[]} solarPanels array of SolarPanel
   */
  getTotalInstallationCost(solarPanels: SolarPanel[]) {
    return solarPanels
      .map(solarPanel => solarPanel.cost > 0 ? solarPanel.cost : 0)
      .reduce((totalValue, currentValue) => totalValue + currentValue);
  }

  /**
   * Return the greater cost installation of the array provided.
   * @param {SolarPanel[]} solarPanels array of SolarPanel
   */
  getGreaterCostInstallation(solarPanels: SolarPanel[]): SolarPanel {
    return solarPanels
      .reduce((previous, current) => {
        return previous.cost > current.cost ? previous : current;
      });
  }

  /**
   * Return the top 3 months with greater installation cost.
   * @param {SolarPanel[]} solarPanels array of SolarPanel
   */
  getTop3MonthInstallations(solarPanels: SolarPanel[]) {
    const installations = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    const countInstallationCost = (solarPanel: SolarPanel) => {
      const month = new Date(solarPanel.installationDate).getMonth();
      installations[month - 1] += 1;
    };
    const filterCurrentYear = solarPanel =>
      new Date(solarPanel.installationDate).getFullYear() === new Date().getFullYear();
    const sortByGreaterCost = (previous, current) =>
      previous.count < current.count ? 1 : -1;

    solarPanels
      .filter(filterCurrentYear)
      .forEach(countInstallationCost);

    return installations
      .map((count, i) => ({ count, month: i + 1 }))
      .sort(sortByGreaterCost)
      .slice(0, 3);
  }

  /**
   * Return total system size grouped by year.
   * @param {SolarPanel[]} solarPanels array of SolarPanel.
   */
  getTotalSystemSizeByYear(solarPanels: SolarPanel[]) {
    const totalSystemSize = {};

    solarPanels.forEach((solarPanel) => {
      const year = new Date(solarPanel.installationDate).getFullYear();

      totalSystemSize[year] = totalSystemSize[year] ?
        totalSystemSize[year] + solarPanel.systemSize :
        solarPanel.systemSize;
    });

    return Object.keys(totalSystemSize).map(key => ({
      year: key, systemSize: totalSystemSize[key],
    }));
  }
}
