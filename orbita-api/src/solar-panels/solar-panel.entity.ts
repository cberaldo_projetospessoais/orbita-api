
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class SolarPanel {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dataProvider: string;

  // mm/dd/yyyy
  @Column()
  installationDate: string;

  @Column()
  systemSize: number;

  @Column()
  zipCode: number;

  @Column({ length: 2 })
  state: string;

  @Column()
  cost: number;
}
