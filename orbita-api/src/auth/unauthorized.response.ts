import { HttpStatus } from '@nestjs/common';

export class UnauthorizedResponse {
  statusCode: number = HttpStatus.UNAUTHORIZED;
  error: string = 'Unauthorized';
}

export const sendUnauthorizedResponse = (res) => {
  res.status(HttpStatus.UNAUTHORIZED).json(new UnauthorizedResponse());
};
