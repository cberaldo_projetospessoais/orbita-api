import { Controller, Post, UseGuards, Req, Get } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  /**
   * Inject controller dependencies.
   * @param authService injected auth services.
   */
  constructor(private readonly authService: AuthService) { }

  @Post('/')
  @UseGuards(AuthGuard('local'))
  login(@Req() req) {
    return this.authService.login(req.user);
  }
}
