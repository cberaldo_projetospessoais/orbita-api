import { Module } from '@nestjs/common';
import { SolarPanelService } from '../solar-panels/solar-panel.service';
import { solarPanelProvider } from '../solar-panels/solar-panel.provider';
import { DatabaseModule } from 'src/database/database.module';
import { SolarPanelImporter } from './solar-panel.importer';

@Module({
  imports: [DatabaseModule],
  providers: [
    ...solarPanelProvider,
    SolarPanelImporter,
    SolarPanelService,
  ],
})
export class SolarPanelModule {}
