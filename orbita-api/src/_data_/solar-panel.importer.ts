import { Injectable, Inject, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { SolarPanel } from 'src/solar-panels/solar-panel.entity';
import * as json from 'big-json';
import * as fs from 'fs';

@Injectable()
export class SolarPanelImporter {
  /**
   * Inject importer dependencies.
   * @param {SolarPanelService} solarPanelService injected service from the module.
   */
  constructor(
    @Inject('SOLAR_PANEL_REPOSITORY')
    private readonly solarPanelsRepository: Repository<SolarPanel>,
  ) {
    Logger.log(`Solar panel importer initialized.`, 'Orbita');
    console.time();

    this.importSolarPanelData()
      .then(() => console.timeEnd());
  }

  async loadSolarPanelData() {
    return new Promise((resolve, reject) => {
      const parseStream = json.createParseStream();
      const data = [];

      const fileName = `${__dirname}/solar_data.json`;
      const readerOptions = { flags: 'r', encoding: 'utf-8' };
      const readStream = fs.createReadStream(fileName, readerOptions);

      parseStream.on('data', (pojo) => {
        Object.keys(pojo).map(async (key) => {
          const solarPanel = pojo[key];
          data.push({
            dataProvider: solarPanel['Data Provider'],
            installationDate: solarPanel['Installation Date'],
            systemSize: solarPanel['System Size'],
            zipCode: solarPanel['Zip Code'],
            state: solarPanel['State'],
            cost: solarPanel['Cost'],
          });
        });
      });

      parseStream.on('end', () => {
        resolve(data);
      });

      readStream.pipe(parseStream);
    });
  }

  async importSolarPanelData() {
    return new Promise((resolve, reject) => {
      this.loadSolarPanelData()
        .then(async (data: []) => {
          while (data.length > 0) {
            const solarPanel = data.shift();
            // console.log(solarPanel);
            await this.solarPanelsRepository.save(solarPanel);
          }

          resolve();
        })
        .catch(error => reject(error));
    });
  }
}
