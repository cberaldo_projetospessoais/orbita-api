import { NestFactory } from '@nestjs/core';
import { SolarPanelModule } from './solar-panel.module';

async function bootstrap() {
  const app = await NestFactory.create(SolarPanelModule);
}
bootstrap();
