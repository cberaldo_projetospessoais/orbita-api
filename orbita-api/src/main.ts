import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { json, urlencoded } from 'body-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const port = process.env.PORT || 3000;

  // allow json requests to endpoints.
  app.use(json());
  // allow url encoded requests to endpoints.
  app.use(urlencoded({ extended: true }));
  // allow cross origin requests to endpoints.
  app.enableCors();

  await app.listen(port, () => {
    Logger.log(`Application is running on http://localhost:${port}`, 'Orbita');
  });
}
bootstrap();
